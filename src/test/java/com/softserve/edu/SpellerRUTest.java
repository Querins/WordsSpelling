package com.softserve.edu;

import com.softserve.edu.numberspelling.NumberSpeller;
import org.junit.Test;

import java.util.Locale;

import static com.softserve.edu.numberspelling.Speller.getSpeller;

import static org.junit.Assert.*;

public class SpellerRUTest {

    private static NumberSpeller s = getSpeller(Locale.forLanguageTag("ru"));

    @Test
    public void spellNumberTestSmallNumber() {

        assertEquals("двенадцать", s.spellNumber(12));

    }

    @Test
    public void spellNumberFor23_760_000() {

        assertEquals("двадцать три миллиона семьсот шестьдесят тысяч", s.spellNumber(23_760_000));

    }

    @Test
    public void spellNumberFor58_760_123() {

        assertEquals("пятьдесят восемь миллионов семьсот шестьдесят тысяч сто двадцать три", s.spellNumber(58_760_123));

    }

    @Test
    public void spellNumberFor99_882_100() {
        assertEquals("девяносто девять миллионов восемьсот восемьдесят две тысячи сто", s.spellNumber(99_882_100));
    }

    @Test
    public void spellNumberFor100_215_881_120() {
        assertEquals("сто миллиардов двести пятнадцать миллионов восемьсот восемьдесят одна тысяча сто двадцать",
                s.spellNumber(100_215_881_120L));
    }

    @Test
    public void spellNumber1000() {
        assertEquals("тысяча", s.spellNumber(1000));
    }

    @Test
    public void spellNumber2000() {
        assertEquals("две тысячи", s.spellNumber(2000));
    }

    @Test
    public void spellNumber2002() {
        assertEquals("две тысячи два", s.spellNumber(2002));
    }

    @Test
    public void spellNumber3_000_000_000() {
        assertEquals("три миллиарда", s.spellNumber(3_000_000_000L));
    }

    public void spellNumberMinus_36_000_000_000() {
        assertEquals("минус тридцать шесть миллиардов",
                s.spellNumber(-36_000_000_000L));
    }



}

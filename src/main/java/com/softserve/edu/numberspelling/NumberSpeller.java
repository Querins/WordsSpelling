package com.softserve.edu.numberspelling;

public interface NumberSpeller {

    String spellNumber(long number);

}

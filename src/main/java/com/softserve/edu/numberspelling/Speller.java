package com.softserve.edu.numberspelling;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

public class Speller {

    public static NumberSpeller getSpeller(Locale locale) {
        NumberSpeller speller;
        try {
            String language = locale.getLanguage().toUpperCase();
            if(language.equals("")) {
                throw new IllegalArgumentException("Argument passed does not match any language");
            }
            String pack = Speller.class.getPackage().getName();
            speller = (NumberSpeller) Class.forName(pack + ".Speller" + language).getMethod("getInstance").invoke(null);
        } catch(ClassNotFoundException e) {
            throw new UnsupportedOperationException("There is no implementation for given locale yet");
        } catch(NoSuchMethodException e) {
            throw new RuntimeException("Every implementation of Speller should be a singleton and provide getInstance() method");
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Problem occurred when trying to load class", e);
        }
        return speller; // this row should not be reached...
    }
}

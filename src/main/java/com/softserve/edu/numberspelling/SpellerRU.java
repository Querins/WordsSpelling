package com.softserve.edu.numberspelling;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Character.*;

class SpellerRU implements NumberSpeller {

    private static Map<Long, String> names = new HashMap<>(42);
    static {

        String[] strNames = new String[] {
                "ноль","один", "два","три","четыре","пять","шесть",
                "семь","восемь","девять","десять","одинадцать",
                "двенадцать","тринадцать","четырнадцать","пятнадцать",
                "шестнадцать","семнадцать","восемнадцать","девятнадцать","двадцать", // ones

                "тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто", // decimals

                "сто","двести","триста","четыреста","пятсот","шестьсот","семьсот", "восемьсот", "девятьсот", // hundreds

                "тысяча","миллион","миллиард","триллион","квадриллион","квинтиллион" // powers of thousand
        };

        // filling ones
        for(long i = 0; i <= 20; i++) {
            names.put(i, strNames[(int)i]);
        }

        // filling decimals
        for(long i = 30, j = 21; j <= 28; i += 10, j++) { // i -- number value, j -- number index in strNames
            names.put(i, strNames[(int)j]);
        }

        // filling hundreds
        for(long i = 100, j = 28; j <= 36; i += 100, j++) {
            names.put(i, strNames[(int)j]);
        }

        for(long i = 1000, j = 37; j <= strNames.length - 1; i *= 1000, j++) {
            names.put(i, strNames[(int)j]);
        }
    }

    private SpellerRU() {}

    private static class InstanceHolder {
        static final NumberSpeller INSTANCE = new SpellerRU();
    }

    public static NumberSpeller getInstance() {
        return InstanceHolder.INSTANCE;
    }

    /**
     * Returns the word representation of the number
     * @param n
     * @return
     */
    public String spellNumber(long n) {

        StringBuilder answer = new StringBuilder(0);

        if(n < 0) {
            answer.append("минус ");
            answer.append(spellNumber(n));
            return answer.toString();
        }

        if(names.containsKey(n)) {
            return names.get(n);
        }

        char[] charNum = Long.toString(n).toCharArray();
        int leadingGroup = charNum.length % 3 == 0 ? 3 : charNum.length % 3; // 54_445 -> 2, 8_000 -> 1, 222_222 -> 3

        char[] firstGroup = Arrays.copyOfRange(charNum, 0, leadingGroup);
        char[] remaining = Arrays.copyOfRange(charNum, leadingGroup, charNum.length);

        String k = spellGroup(firstGroup);

        if(charNum.length > leadingGroup) {
            if( remaining.length == 3 ) { // thousands are special case: ОДНА тысяча and ДВЕ тысячи
                int lastIndex = firstGroup.length - 1;
                char c = firstGroup[lastIndex];
                switch (firstGroup[lastIndex]) {
                    case '1':
                        firstGroup[lastIndex] = '0';
                        k = spellGroup(firstGroup) + " одна";
                        firstGroup[lastIndex] = c;
                        break;
                    case '2':
                        firstGroup[lastIndex] = '0';
                        k = spellGroup(firstGroup) + " две";
                        firstGroup[lastIndex] = c;
                        break;
                }
            }
            String thousands = modifyPowerOfThousand(firstGroup, remaining.length);
            long remainingNumber = Long.parseLong(new String(remaining));
            answer.append(k);
            answer.append(' ');
            answer.append(thousands);
            answer.append(' ');
            answer.append(remainingNumber == 0 ? "" : spellNumber(remainingNumber));
            return answer.toString().trim();

        } else {
            return k;
        }

    }

    private String spellGroup(char[] str) {

        long numGroup = Long.valueOf(new String(str));
        if(numGroup == 0) {
            return "";
        }

        if(str.length > 3 || str.length < 1) {
            throw new IllegalArgumentException("Illegal length of char[]: " + str.length);
        }

        if(str.length == 1 ) {
            return names.get((long)getNumericValue(str[0]));
        }

        StringBuilder answer = new StringBuilder();

        if(str.length == 2) {
            if(names.containsKey(numGroup)) {
                return names.get(numGroup);
            }
            answer.append(names.get((long)getNumericValue(str[0]) * 10));
            answer.append(' ');
            answer.append(names.get((long)getNumericValue(str[1])));
            return answer.toString();
        }

        for(int i = 0; i < str.length; i++) {
            int n = Character.getNumericValue(str[i]);
            if(n == 0) {
                continue;
            }
            if(i == 1 || i == 2) {
                answer.append(' ');
            }
            if(i == 1 && n == 1) { // if second number is 1 then it'a 10..20 -> search in map
                answer.append(names.get(Long.valueOf(new String(str, 1, 2)))); // value is in map
                return answer.toString();
            }
            answer.append(names.get((long)(n * Math.pow(10, 2 - i))));
        }
        return answer.toString();
    }

    private String modifyPowerOfThousand(char[] prefix, int pow) { // тысячи, миллиона, миллиарда

        int numPrefix = Integer.parseInt(new String(prefix));
        long number = (long)Math.pow(10, pow);
        int length = names.get(number).length();

        if( prefix.length > 3 ) {
            throw new IllegalArgumentException("Invalid argument for modifyPowerOfThousand method: " + new String(prefix));
        }

        if(numPrefix == 1) {
            return names.get(number); // одна тысяча, один миллион
        }

        if(numPrefix > 20) {
            return modifyPowerOfThousand(new char[] {prefix[prefix.length - 1]}, pow);
        }

        if(pow == 3) { // thousand is special case
            if(numPrefix >= 2 && numPrefix <= 4) {
                return names.get(number).substring(0, length - 1) + "и"; // две тысячи
            }
            if(numPrefix == 0 || numPrefix > 4) { // 5,6,7...20
                return names.get(number).substring(0, length - 1); //пять тысяч
            }
        } else {

            if(numPrefix >= 2 && numPrefix <= 4) {
                return names.get(number) + "а"; // два миллиарда
            }
            if(numPrefix == 0 || numPrefix > 4) { // 5,6,7...
                return names.get(number) + "ов"; //пять миллионов
            }

        }

        return "";
    }

}

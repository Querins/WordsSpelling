package com.softserve.edu;

import com.softserve.edu.numberspelling.Speller;

import java.util.Locale;

public class Main {

    public static void main(String[] args) {

        switch(args.length) {

            case 0:
                System.out.println("This program prints russian word for number" +
                        ", passed in first parameter, any other parameters will be ignored");
            default:
                try {
                    long number = Long.valueOf(args[0]);
                    System.out.println(Speller.getSpeller(Locale.forLanguageTag("")).spellNumber(number));
                } catch (NumberFormatException e) {
                    System.out.println("Bad format of number");
                }
        }

    }

}
